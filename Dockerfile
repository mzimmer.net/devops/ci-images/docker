FROM docker:stable

RUN set -euxo pipefail; \
    apk update; \
    apk add --no-cache \
        ca-certificates \
        curl \
        git \
        make \
        wget;
